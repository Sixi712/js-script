// ==UserScript==
// @name         webp2png
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Luo Shiheng
// @include      https://juejin.im/post/*
// @grant        none
// ==/UserScript==

(function() {
  'use strict';
  // Your code here...
 
  function getDataFromUrl(url) {
    return new Promise(resolve => {
      let request = new XMLHttpRequest();
      request.open('GET', url, true);
      request.responseType = 'blob';
      request.onload = () => {
          let reader = new FileReader();
          reader.readAsDataURL(request.response);
          reader.onload = e => {
            resolve(e.target.result)
          };
      };
      request.send();
    })

  };

  function toPNG(img) {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    return new Promise(resolve=>{
      let imageEle = new Image();
      imageEle.src = img;
      imageEle.onload = () => {
        canvas.width = imageEle.width;
        canvas.height = imageEle.height;
        ctx.drawImage(imageEle, 0, 0);
        resolve(canvas.toDataURL('image/png'));
      }
    })
  };

  function getImage() {
    const allImage = document.querySelectorAll("img");
    const webpImageList = Array.from(allImage).filter(item=>item.src.toLowerCase().includes("webp"));
    if(webpImageList.length) {
      webpImageList.forEach(item => {
        if(item.src.toLowerCase().startsWith("data:image/")) {
          toPNG().then(res=>{
            item.src = res;
          })
        }else {
          getDataFromUrl(item.src)
            .then(res => toPNG(res))
            .then(res => item.src = res)
        }
      })
    }
  }
  getImage()
})();