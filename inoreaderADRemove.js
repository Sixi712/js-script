// ==UserScript==
// @name         Inoreader AD Remove
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Sixi712
// @match        https://www.inoreader.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    let checkNode = null;
    let chackDialog = null;
    let hideDuplicate = null;
    checkNode = setInterval(()=>{
    if(document.getElementsByClassName("ad_size_leaderboard").length){
        document.getElementsByClassName("ad_size_leaderboard")[0].style.display = "none";
        document.getElementsByClassName("ad_title")[0].style.display = "none";
        document.getElementById("sinner_container").style.display = "none";
        document.getElementById("reader_pane").style.paddingRight = 0;
        clearInterval(checkNode);
         }
    }, 1000)
    chackDialog = setInterval(()=>{
        if(document.getElementsByClassName("inno_dialog").length){
            window.adb_close()
        }

    }, 1000)
    hideDuplicate = setInterval(()=>{
        let finalList = [];
        let originArticle = document.getElementsByClassName("article_unreaded")
        document.getElementById("sinner_container").style.display = "none"
        for(let i in originArticle){
            if(!finalList.includes(originArticle[i].innerText.split("\n")[1])){
                finalList.push(originArticle[i].innerText.split("\n")[1])
            }else {
                originArticle[i].style.display = "none"
            }
        }
    }, 10000)
})();